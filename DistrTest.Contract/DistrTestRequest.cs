﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Ccr.Core;

namespace DistrTest.Contract {
    [Serializable]
    public class DistrTestRequest {

        public string Id { get; set; }
        public byte[] File { get; set; }
        public List<byte[]> Dependencies { get; set; } 

        public List<string> TestMethods { get; set; }

        public Port<TestStarted> TestStarted { get; set; }

        public Port<TestFailure> TestFailure { get; set; }

        public Port<TestSuccess> TestSuccess { get; set; }

    }
}

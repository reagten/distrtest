﻿using System;

namespace DistrTest.Contract
{
    [Serializable]
    public abstract class TestMessage
    {
        public string RequestId { get; set; }
        public string TestName { get; set; }
    
    }
}
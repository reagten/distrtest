using System;

namespace DistrTest.Contract
{
    [Serializable]
    public class TestFailure: TestMessage
    {
        public string Details { get; set; }
    }
}
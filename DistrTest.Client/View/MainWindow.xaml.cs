﻿using DistrTest.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using XcoAppSpaces.Core;
using DistrTest.Client.View;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.Diagnostics;
using Path = System.IO.Path;

namespace DistrTest.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Closed += (s, e) => App.Current.Shutdown();

            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                for(int i=1;i<args.Length;i++)
                {
                    ServerTextBox.AppendLine(args[i].Trim());
                }
            }
            else
            {
                ServerTextBox.AppendLine("localhost:8000");
            }

            this.Dispatcher.Invoke(() =>
            {
                string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\defaultUnitTests\\UnitTestProject1.dll");
                selectedFile = File.ReadAllBytes(path);
                var testMethods = this.getTestMethodsFromAssembly(selectedFile);

                foreach (var testMethod in testMethods)
                {
                    LogTest(testMethod);
                }
            });
        }

        private byte[] selectedFile;

        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".dll";
            dlg.Filter = "Assembly (.dll)|*.dll";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                FileNameTextBox.Text = filename;
                selectedFile = File.ReadAllBytes(dlg.FileName);
            }
        }

        public delegate void UpdateTextCallback(string message);

        private List<string> getTestMethodsFromAssembly(byte[] assemblyName)
        {
            var testAssembly = Assembly.Load(assemblyName);
            
            return testAssembly.GetTypes().ToList().SelectMany(type => {
                return type.GetMethods().Where(m => m.GetCustomAttributes().OfType<NUnit.Framework.TestAttribute>().Any()).ToList().Select(method => {
                    string qualifiedName = type.FullName + "." + method.Name;
                    return qualifiedName;
                });
            }).ToList();
        }

        private List<byte[]> getDependenciesFromAssembly(byte[] assemblyName) {
            var testAssembly = Assembly.Load(assemblyName);
            return new List<byte[]>();
            //return testAssembly.GetReferencedAssemblies()
            //    .Select(dep =>
            //    {
            //        var assm = AppDomain.CurrentDomain.Load(dep);
            //        return File.ReadAllBytes(assm.Location);
            //    }).ToList();
        }

        private void TestThread()
        {
            this.Dispatcher.Invoke((Action)(() => { LogBox.Clear();  }));

            using (var space = new XcoAppSpace("tcp.port=0"))
            {
                Log("reading file...");
                var file = selectedFile;

                Log("analyzing assembly...");
                
                Log("building message...");

                var requestToServerAssignment = new Dictionary<string, string>();
                var testMethods = getTestMethodsFromAssembly(selectedFile);
                var dependencies = getDependenciesFromAssembly(selectedFile);

                var countDown = new CountdownEvent(testMethods.Count);
                var serverQueue = new BlockingCollection<string>();

                //--- set up response port
                var testStarted = space.Receive<TestStarted>(s =>
                {
                    var server = requestToServerAssignment[s.RequestId];
                    LogTest(String.Format("{1}: {0} started...", s.TestName, server));
                });
                var testFailure = space.Receive<TestFailure>(s =>
                {
                    var server = requestToServerAssignment[s.RequestId];
                    LogTest(String.Format("{2}: {0} failed: {1}", s.TestName, s.Details, server));
                    var requestId = s.RequestId;
                    if (requestToServerAssignment.Remove(requestId)) {
                        countDown.Signal();
                        serverQueue.Add(server);
                    }
                });
                var testSuccess = space.Receive<TestSuccess>(s => {
                    var server = requestToServerAssignment[s.RequestId];
                    LogTest(String.Format("{1}: {0} finished successfully", s.TestName, server));
                    var requestId = s.RequestId;
                    if (requestToServerAssignment.Remove(requestId))
                    {
                        countDown.Signal();
                        serverQueue.Add(server);
                    }
                });

                this.Dispatcher.Invoke((Action)(() =>
                {
                    StringCollection lines = new StringCollection();
                    int lineCount = ServerTextBox.LineCount;

                    for (int line = 0; line < lineCount; line++)
                        serverQueue.Add(ServerTextBox.GetLineText(line).Trim());
                }));

                var workers = serverQueue.ToDictionary(sq => sq, sq => space.ConnectWorker<DistrTestWorker>(sq, sq));

                var sw = Stopwatch.StartNew();
                foreach (var testMethodFullName in testMethods)
                {
                    var server = serverQueue.Take();

                    var requestId = Guid.NewGuid().ToString();
                    var msg = new DistrTestRequest()
                    {
                        Id = requestId,
                        File = file,
                        Dependencies = dependencies,
                        TestMethods = new List<string> { testMethodFullName },
                        TestSuccess = testSuccess,
                        TestFailure = testFailure,
                        TestStarted = testStarted
                    };
                    Log("connecting to server: "+ server);
                    var worker = workers[server];
                    requestToServerAssignment.Add(requestId, server);
                    Log("posting message...");
                    worker.Post(msg);
                    Log("done.");
                }
                countDown.Wait();
                sw.Stop();
                LogTest(string.Format("executing tests done in {0}", sw.Elapsed));
                Console.ReadLine();
            }
        }

        private void LogTest(string text)
        {
            LogBox.Dispatcher.Invoke(new UpdateTextCallback(this.UpdateText), text);
        }

        private void Log(string text)
        {
            //LogBox.Dispatcher.Invoke(new UpdateTextCallback(this.UpdateText), text);
        }

        private void UpdateText(string message)
        {
            LogBox.AppendLine(message);
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            Thread test = new Thread(new ThreadStart(TestThread));
            test.Start();
        }
    }
}

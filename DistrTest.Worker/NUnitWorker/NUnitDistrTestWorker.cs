using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DistrTest.Contract;
using NUnit.Core;
using NUnit.Core.Filters;
using XcoAppSpaces.Core;

namespace DistrTest.Worker.NUnitWorker {
    class NUnitDistrTestWorker : DistrTestWorker
    {
        private const int TimeoutMilliseconds = 60*1000;
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(4);

        [XcoConcurrent]
        void ProcessDistrTestRequest(DistrTestRequest request)
        {
            _semaphore.Wait();
            try
            {
                var testExecution = Task.Run(() => ProcessDistrTestRequestInternal(request));
                testExecution.Wait(TimeoutMilliseconds);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                foreach (var testMethod in request.TestMethods)
                {
                    request.TestFailure.Post(new TestFailure
                    {
                        RequestId = request.Id,
                        TestName = testMethod,
                        Details = e.Message
                    });
                }
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private static void ProcessDistrTestRequestInternal(DistrTestRequest request)
        {
            var requestId = request.Id;

            var file = request.File;
            var testMethods = request.TestMethods;

            Console.WriteLine("{0} > processing test request... ", requestId);
            Console.WriteLine("{0}  > test assembly size is {1} bytes ", requestId, file.Length);
            Console.WriteLine("{0}  > number of tests to execute {1} ", requestId, testMethods.Count());

            ExecuteTests(request);

            Console.WriteLine("{0} > running tests done. ", requestId);
            Console.WriteLine("{0} > processing test request done.", requestId);
        }

        private static void ExecuteTests(DistrTestRequest request)
        {
            var requestId = request.Id;

            Console.WriteLine("{0} > initializing NUnit test framework... ", requestId);

            var filter = new OrFilter();
            foreach (var qualifiedMethodName in request.TestMethods)
            {
                filter.Add(new SimpleNameFilter(qualifiedMethodName));
            }

            CoreExtensions.Host.InitializeService();
            // http://stackoverflow.com/questions/13207289/nunit-api-and-running-tests-programmatically
            var assembly = Assembly.Load(request.File);
            request.Dependencies.ForEach(dep => Assembly.Load(dep));
            TestExecutionContext.CurrentContext.TestPackage = new TestPackage(assembly.GetName().FullName);

            var testSuite = GetTestSuiteFromAssembly(assembly);
            testSuite.Run(
                new DistrTestEventListener {Request = request}, 
                filter
            );
        }

        protected static TestSuite GetTestSuiteFromAssembly(Assembly assembly) {
            var treeBuilder = new NamespaceTreeBuilder(
                new TestAssembly(assembly, assembly.GetName().FullName));
            treeBuilder.Add(GetFixtures(assembly));
            return treeBuilder.RootSuite;
        }

        protected static IList GetFixtures(Assembly assembly) {
            return assembly.GetTypes()
                .Where(TestFixtureBuilder.CanBuildFrom)
                .Select(TestFixtureBuilder.BuildFrom)
                .ToList();
        }

        private static string CreateTmpTestAssemblyFile(string testExecutionId, byte[] file)
        {
            var tmpFileName = Path.GetTempPath() + "test-" + testExecutionId + ".dll";
            Console.WriteLine("{0} > saving test assembly to tmp file at {1} ...", testExecutionId, tmpFileName);
            File.WriteAllBytes(tmpFileName, file);
            return tmpFileName;
        }
    }
}
using System;
using DistrTest.Contract;
using NUnit.Core;

namespace DistrTest.Worker.NUnitWorker
{
    class DistrTestEventListener : EventListener
    {

        public DistrTestRequest Request { get; set; }

        public void RunStarted(string name, int testCount)
        {
            Console.WriteLine("RunStarted");
        }

        public void RunFinished(TestResult result)
        {
            Console.WriteLine("RunFinished");
        }

        public void RunFinished(Exception exception)
        {
            Console.WriteLine("RunFinished");
        }

        public void TestStarted(TestName testName)
        {
            Request.TestStarted.Post(new TestStarted
            {
                RequestId = Request.Id,                
                TestName = testName.FullName
            });
        }

        public void TestFinished(TestResult result)
        {
            if (result.IsSuccess)
            {
                Request.TestSuccess.Post(new TestSuccess
                {
                    RequestId = Request.Id,
                    TestName = result.FullName
                });
            }
            else
            {
                Request.TestFailure.Post(new TestFailure
                {
                    RequestId = Request.Id,
                    TestName = result.FullName, 
                    Details = result.Message
                });
            }
        }

        public void SuiteStarted(TestName testName)
        {
            Console.WriteLine("SuiteStarted");
        }

        public void SuiteFinished(TestResult result)
        {
            Console.WriteLine("SuiteFinished");
            
        }

        public void UnhandledException(Exception exception)
        {
            Console.WriteLine("UnhandledException");
        }

        public void TestOutput(TestOutput testOutput)
        {
            Console.WriteLine("TestOutput");
        }
    }
}
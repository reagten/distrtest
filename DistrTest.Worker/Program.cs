﻿using System;
using DistrTest.Contract;
using DistrTest.Worker.NUnitWorker;
using XcoAppSpaces.Core;

namespace DistrTest.Worker {
    class Program {
        static void Main(string[] args)
        {
            var serverPort = args.Length > 0 ? args[0] : "8000";

            Console.WriteLine("starting server...");
            using (var space = new XcoAppSpace("tcp.port=" + serverPort)) {
                Console.WriteLine("server listening on port {0}", serverPort);
                space.RunWorker<DistrTestWorker, NUnitDistrTestWorker>();
                Console.WriteLine("server started.");
                Console.WriteLine("waiting for messages...");
                Console.ReadLine();
            }
        }
    }
}

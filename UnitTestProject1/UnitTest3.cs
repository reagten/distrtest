﻿using System;
using System.Threading;

namespace UnitTestProject1 {

    using NUnit.Framework;

    [TestFixture]
    public class UnitTest3 {
        [Test]
        public void TestMethod1() {
            Assert.AreEqual(1,1);
        }

        [Test]
        public void TestMethod2() {
            Assert.AreEqual(1, 1);
        }

        [Test]
        public void TestMethod3() {
            Assert.AreEqual(2, 1);
        }

        [Test]
        public void TestMethodLongRunning1() {
            Thread.Sleep(4000);
            Assert.AreEqual(1, 1);
        }

        [Test]
        public void TestMethodLongRunning2() {
            Thread.Sleep(2000);
            Assert.AreEqual(1, 1);
        }

        [Test]
        public void TestMethodLongRunning3() {
            Thread.Sleep(6000);
            Assert.AreEqual(1, 1);
        }
    }
}
